package tigase.disteventbus;

/**
 *
 * @deprecated Please use {@link tigase.disteventbus.clustered.EventHandler}
 *             instead. This insterface is only for backward compatibility.
 */
@Deprecated
public interface EventHandler extends tigase.disteventbus.clustered.EventHandler {

}
